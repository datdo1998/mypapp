/**
 * @return {number}
 */
function Add(str) {
    const validation = /^\/\/\W$/m;
    let string2 = str.slice(str.indexOf('\n') + 1);
    if (validation.test(str)){
        const string  =  str.match(validation).join('').split('//').filter(item => item !== '').join('');
        if (string2.indexOf(string) !== -1){
            return string2.split(string).map(Number).filter( value => !Number.isNaN(value)).reduce((a, b) => a + b);
        }else {
            return 0;
        }
    }else {
        return string2.match(/[+-]?\d+(\.\d+)?/g).map(Number).reduce((a,b) => a + b);
    }
};
console.log(Add('//\n1.2.5'));
